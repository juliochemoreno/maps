import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { NavController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

declare var google;

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  @ViewChild('map') mapElement: ElementRef;

  map: any; marker: any;

  constructor( public navCtrl: NavController, public geo: Geolocation) {
  }

  ionViewDidLoad(){
    this.loadMap();
  }

  loadMap(){

    this.geo.getCurrentPosition().then( pos => {

      let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

      this.marker = new google.maps.Marker({
        position: latLng,
        map: this.map,
        icon: image,
        title:"Mi ubicación"
      });

    }).catch( err => console.log(err));
  }

  positionMarker() {
    this.map.panTo(this.marker.getPosition());
  }
}
